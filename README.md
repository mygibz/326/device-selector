# GIBZ - Device Selector

**The perfect application to select the ideal device for your apprenticeship!**

Select your device and check if it's compatible with your profession 
or get device recommendations that meet the requirements

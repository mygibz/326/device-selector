# ToDo

**Basics**

- [x] Running Electron application
  
**Frontend**
- [x] Static page structure
- [x] Tabs to switch between compatibility check and recommendation
- [x] Profession-Selector

*Compatibility-Check Tab*

- [ ] Working device search bar to search for a device
- [x] Overall compatibility display
- [x] Advanced section to edit specs
  - [x] ...with separate compatibility display

*Recommendations Tab*

- [x] List with laptop recommendations including
  - [x] Price
  - [x] Compatibility display
  - [ ] Accessories (e.g. pen)
  - [ ] Direct link to product
  - [x] Name 
  - [ ] Description
  - [x] Product image

**Backend**

- [x] Ability to fetch data from the device API
- [x] Ability to fetch specs from json file
- [x] Ability to compare specs and return a compatibility percentage
- [x] "Device" class
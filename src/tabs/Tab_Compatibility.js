import React from "react";
import Spec from '../components/Spec'
import Meter from '../components/Meter'
import { calculateTotalCompatibility } from '../services/compatibilityCalculatorService'
import { Input, Spacer, Flex, Box, SlideFade, Button, Divider } from "@chakra-ui/react"
import sampleSpec from '../sample-spec.json'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Image,
  Text
} from "@chakra-ui/react"
import { Search2Icon, ExternalLinkIcon } from "@chakra-ui/icons";

const apiLocation = "https://devicechecker.any.gay"

const processData = profession => {
  const spec = sampleSpec[profession].spec
  const specsToCheck = [...new Set([...Object.keys(spec.required), ...Object.keys(spec.recommended)])]

  let requiredData = {}
  specsToCheck.forEach(s => requiredData[s] = spec.required[s])

  const recommendedData = {}
  specsToCheck.forEach(s => recommendedData[s] = spec.recommended[s] || spec.required[s])

  return [ specsToCheck, requiredData, recommendedData ]
}

let [ specsToCheck, requiredData, recommendedData ] = processData(0)

function Advanced({ specs, specCallback }) {
  const [show, setShow] = React.useState(false)

  return (
    <>
      {!show && <Button onClick={() => setShow(!show)} mt="1rem">
        Advanced
      </Button>}

      <SlideFade in={show}>
        <Divider mt={5} mb={5} />
        <Box pl={20} pr={20}>
          {specs.map(({ spec, inDeviceValue }) => <Spec key={spec + inDeviceValue} spec={spec} requiredData={requiredData} recommendedData={recommendedData} inDeviceValue={inDeviceValue} callback={specCallback}/> )}
        </Box>
      </SlideFade>
    </>
  )
}

let globalCompatibilityScoreSpecs = new Map()
const MainContent = ({ laptopResults, changeNotebook }) => {
  const [ globalCompatibilityScore, updateGlobalCompatibilityScore ] = React.useState(0)
  const [ globalCompatibilityStatus, updateGlobalCompatibilityStatus ] = React.useState("red")
  const [ searchResults, updateSearchResults ] = React.useState([])
  
  const specs = specsToCheck.map(spec => {return { spec, inDeviceValue: laptopResults[spec] }})

  const updateGlobalCompatibilityScoreSpec = (spec, percentage, status) => {
    if (globalCompatibilityScoreSpecs.has(spec))
      globalCompatibilityScoreSpecs.delete(spec)
    
    globalCompatibilityScoreSpecs.set(spec, { percentage, status })

    const globalCompatibilityData = calculateTotalCompatibility(globalCompatibilityScoreSpecs)

    updateGlobalCompatibilityScore(globalCompatibilityData.percentage)
    updateGlobalCompatibilityStatus(globalCompatibilityData.status)
  }

  const search = name => {
    fetch(`${apiLocation}/search/${name}`)
    .then(res => res.json())
    .then(data => {
      console.log(`Raw Data from API for notebook with name: '${name}'`, data)
      updateSearchResults(Object.values(data.result))
    })
  }

  const { isOpen, onOpen, onClose } = useDisclosure()

  return <>
    <Box ml={20} mr={20} mb={5} display="flex" alignItems="center">

      <Spacer/>

    </Box>
    <Flex ml={20} mr={20} align="center" alignItems="center">
      <Image src={laptopResults.thumbnail_url} h={50}></Image>
      <Text marginX={3}>{laptopResults.noteb_name || "Unknown model"}</Text>
      <Button onClick={onOpen}><Search2Icon/></Button>
      {navigator.userAgent.includes("Electron") &&
        <Button onClick={loadMyNotebook}><ExternalLinkIcon/></Button>
      }

      <Spacer/>
      
      <Text textAlign="right" mr={3}>Overall Compatibility</Text>
      <Meter progress={globalCompatibilityScore} color={globalCompatibilityStatus} />
    </Flex>

    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent background="gray.100">
        <ModalHeader>Search for a device</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Flex>
            <Input type="search" background="white" id="searchBar" onKeyDown={e => e.keyCode == 13 && search( document.querySelector("#searchBar").value )}/>
            <Button colorScheme="purple" ml={3} onClick={() => search( document.querySelector("#searchBar").value )}> <Search2Icon/> </Button>
          </Flex>
          <Box mt={5} overflowY="scroll" maxH="60vh">
            {searchResults?.map(result => <Flex mb={3} p={3} borderRadius={5} background="white" boxShadow="base" onClick={() => changeNotebook(result.model_info[0].id)} cursor="pointer">
              <Image w="160px" h="100px" src={result.model_resources.thumbnail} />
              <Box>
                <Text>{result.model_info[0].name}</Text>
                <Text>{result.model_info[0].submodel_info.join(" ")}</Text>
              </Box>
            </Flex>)}
          </Box>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="purple" onClick={onClose}>
            Cancel
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>

    <Advanced specs={specs} specCallback={updateGlobalCompatibilityScoreSpec}/>
  </>
}

export const reloadProfession = profession => {
  const [ newSpecsToCheck, newRequiredData, newRecommendedData ] = processData(profession)

  specsToCheck = newSpecsToCheck
  requiredData = newRequiredData
  recommendedData = newRecommendedData
  
  globalCompatibilityScoreSpecs = new Map()
}

let loadMyNotebook;

export default class App extends React.Component {
  state = {
    laptopResults: []
  }

  loadMyNotebook = async () => {
    const request = await fetch('http://localhost:8260')
    const results = await request.json()
    console.log(results)
    this.setState({ laptopResults: results })
  }

  constructor({ notebookID, changeNotebook }) {
    super()

    loadMyNotebook = this.loadMyNotebook

    if (notebookID == "my")
      this.loadMyNotebook()

    else {
      fetch(`${apiLocation}/laptop/${notebookID}`)
      .then(res => res.json())
      .then(data => {
        console.log(`Raw Data from API for notebook with id: '${notebookID}'`, data)
        this.setState({ laptopResults: data })
      })

      this.changeNotebook = changeNotebook
    }
  }

  render () {
    return <MainContent laptopResults={this.state.laptopResults} changeNotebook={this.changeNotebook}/>
  }
}
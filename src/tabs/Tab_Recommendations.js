import React from "react";
import { Flex, Image, SimpleGrid, Text, Progress, Spacer } from "@chakra-ui/react"
import sampleSpec from '../sample-spec.json'
import { calculateComponentCompatibility } from '../services/compatibilityCalculatorService'
import { humanizeSpec } from "../services/humanizerService";

const Meter = ({ progress, status }) => 
  <Progress colorScheme={status} height="32px" value={progress} borderBottomRadius={13}>test</Progress>


const processData = profession => {
  const spec = sampleSpec[profession].spec
  const specsToCheck = [...new Set([...Object.keys(spec.required), ...Object.keys(spec.recommended)])]

  let requiredData = {}
  specsToCheck.forEach(s => requiredData[s] = spec.required[s])

  let recommendedData = {}
  specsToCheck.forEach(s => recommendedData[s] = spec.recommended[s] || spec.required[s])

  return [ specsToCheck, requiredData, recommendedData ]
}

export default class App extends React.Component {
  state = {
    data: []
  }

  constructor() {
    super()

    fetch(`https://devicechecker.any.gay/recommendations`)
    .then(res => res.json())
    .then(data => {
      console.log('Recommendations from API:', data)

      data.forEach((_, i) => {
        let [ specsToCheck, requiredData, recommendedData ] = processData(this.props.profession)

        const scores = specsToCheck.map(spec => {
          
          let { percentage } = calculateComponentCompatibility(data[i][spec], requiredData[spec], recommendedData[spec], humanizeSpec(spec)[1])

          const status = (data[i][spec] < requiredData[spec]
            || (humanizeSpec(spec)[1] == "bool" && !data[i][spec]))
              ? "red"
              : (data[i][spec] < recommendedData[spec])
                ? "orange"
                : "green"

          if (humanizeSpec(spec)[1] == "bool" && status == "red")
            percentage = 0

          return { percentage, status }
        })

        const red = scores.filter(v => v.status == "red").length
        const orange = scores.filter(v => v.status == "orange").length

        data[i].compatibility = (scores.reduce((a, b) => a + b.percentage, 0) / scores.length).toFixed()
        data[i].status = red && "red" || orange && "orange" || "green"
        
      })
      
      this.setState({ data })
    })
  }

  handleClick = device => {
    this.props.changeNotebook(device);
  }

  render() {
    return (
      <SimpleGrid minChildWidth="300px" spacing="1em">
        {this.state.data.filter(d => d.compatibility != "NaN").sort((a, b) => b.compatibility - a.compatibility).map(device => (
          <Flex onClick={() => this.handleClick(device.id)} key={device.id} shadow="md" m={5} borderRadius={13} sx={{cursor: "pointer"}} flexDir="column">

            <Spacer/>
            <Image borderTopRadius={13} src={device.thumbnail_url} alt={device.noteb_name} />
            <Spacer/>

            <Flex justifyContent="space-between">
              <Text fontSize="xl" p={2}>{device.noteb_name}</Text>
              <Text fontSize="l" p={2}>{((Number(device.min_price) + Number(device.max_price)) / 2).toFixed()} CHF</Text>
            </Flex>
            
            <Meter progress={device.compatibility} status={device.status}/>
          </Flex>
        ))}
      </SimpleGrid>
    );
  }
}
export const humanizeSpec = spec => {
  switch (spec) {
    case "processor_core_count":
      return ["Core Count"]
    case "processor_frequency_mhz":
      return ["CPU Frequency", "GHz"]
    case "memory_mb":
      return ["Memory Capacity", "GB"]
    case "storage_capacity_mb":
      return ["Storage Capacity", "GB"]
    case "storage_type":
      return ["Storage Type", "storageType"]
    case "display_size_inch":
      return ["Display Size", "inch"]
    case "touchscreen":
      return ["Touchscreen", "bool"]
    case "pen":
      return ["Pen", "bool"]
    case "usb_c":
      return ["USB Type C", "bool"]
  
    default:
      return [spec];
  }
}

// TODO: make more compact
const humanizeValueHelper = (value, spec, targetUnit) => {
  if (spec.endsWith("_mb") && targetUnit == "GB")
    return value /= 1024
  if (spec.endsWith("_mhz") && targetUnit == "GHz")
    return value /= 1000

  return value
}
export const humanizeValue = (value, spec, targetUnit) => !targetUnit ? value : parseFloat(humanizeValueHelper(value, spec, targetUnit)).toFixed(2)

export const dehumanizeValue = (value, spec, sourceUnit) => {
  if (!sourceUnit) return value

  if (sourceUnit == "GHz" &&  spec.endsWith("_mhz"))
    return value *= 1000
  if (sourceUnit == "GB"  &&  spec.endsWith("_mb"))
    return value *= 1024

  return value
}